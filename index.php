<?php include 'inc/header.php' ?>

	<!-- swiper -->
    <div class="swiper-container has-slider">
        <div class="swiper-wrapper">
        	<?php for ($i=1; $i<=4 ; $i++) {  ?>
        		<div class="swiper-slide">
        			<div class="overlay"></div>
        			<div class="alyka-container">
        				<div class="text-container slider-head fadeInUp animated-delay1">
	        				<div class="slide-box-con">
	        					<div class="fn1">INDIVIDUAL</div>
	        					<div class="fn2">programs</div>
	        					<div class="fn3">Empowering Confident Leaders for Change</div>
	        					<a href="index.php"><div class="alyka-btn slider-btn">find out more</div></a>
	        				</div>
	        			</div>
        			</div>
	            	<div class="slider-bg" style="background-image:url(img/slider/s1.jpg)"></div>
	            </div>
        	<?php } ?>
        </div>
        <!-- Add Pagination -->
        <div class="alyka-container">
        	<div class="swiper-pagination"></div>
        </div>
        <!-- Add Arrows -->
        <div class="assel arrow-right"></div>
        <div class="assel arrow-left"></div>
    </div>
    <div class="content">
    	<section class="about">
    		<div class="alyka-container">
    			<div class="copy-top-bg"></div>
    			<div class="body-content">
    				<h2 class="head2 wow fadeInUp">what we do</h2>
    				<p class="wow fadeInUp">At Outback Initiatives, we care about giving you a challenging but stimulating learning experience that changes how you see yourself and engage with others, personally and professionally.</p>

    				<p class="wow fadeInUp">Outback Initiatives is an internationally renowned human resources consultancy based in Perth, Western Australia. We have been successfully delivering high impact team building and leadership development programs to clients all over the world for 20 years.</p>

    				<a href="index.php"><div class="alyka-btn find-out wow fadeInUp">find out more about what we do</div></a>
    			</div>
    			<div class="body-content whoarewe wow fadeInUp">
    				<h2 class="head2-bold">who are we?</h2>
    				<p class="fn30">We have programs crafted to cater to the various needs of different Individuals and Organisations. Select the category that best suits you to find out more.</p>

    				<div class="head2-bold sub-title wow fadeInUp">Organisation</div>
    			</div>
    			<div class="watwedo-tiles-container">
					<div class="tile-con wow fadeInUp">
						<div class="tile t1"></div>
					</div>
					<div class="tile-con wow fadeInUp">
						<div class="tile t2"></div>
					</div>
					<div class="tile-con wow fadeInUp">
						<div class="tile t3"></div>
					</div>
				</div>
				<div class="head2-bold sub-title wow fadeInUp">individual</div>
				<div class="watwedo-tiles-container">
					<div class="tile-con wow fadeInUp">
						<div class="tile t4"></div>
					</div>
					<div class="tile-con wow fadeInUp">
						<div class="tile t5"></div>
					</div>
					<div class="tile-con wow fadeInUp">
						<div class="tile t6"></div>
					</div>
				</div>
    		</div>
    	</section>
    	<section class="video">
    		<div class="text-con">
    			<h2 class="head2">invest in yourself</h2>
    			<div class="desc">Sed ut vestibulum orci. Nunc venenatis, neque non ullamcorper placerat, velit eros auctor sapien, vulputate ullamcorper mauris urna faucibus tellus.</div>
    			<div class="alyka-btn-drk video-btn">watch video</div>
    		</div>
    	</section>
    	<section class="testimonials"> 
    		<div class="testimonials-container">
    			<h2 class="head2-bold">Testimonails</h2>
	    		<!-- swiper -->
			    <div class="swiper-container-testimonials">
			        <div class="swiper-wrapper">
			        	<?php for ($i=1; $i<=3 ; $i++) {  ?>
			        		<div class="swiper-slide testi">
			        			<div class="alyka-container">
			        				<div class="text-container testi">
			        					<div class="testimonials-copy">“I can sincerely say that this initiative has had a huge impact on people’s lives...”</div>
			        						<div class="desc">Lorem ipsumin</div>
			        						<div class="alyka-btn-drk testi-btn">read more</div>
				        			</div>
			        			</div>
				            </div>
			        	<?php } ?>
			        </div>		        <!-- Add Arrows -->
			        <div class="assel arrow-right testi"></div>
			        <div class="assel arrow-left testi"></div>
		    	</div>
    		</div>
    		<div class="testi-bg"></div>
    		<div class="alyka-container">
    			<div class="testimonail-tiles-con clearfix">
	    			<div class="testimonail-tile left">
	    				<div class="testi-tile-big tile-bg1 wow fadeInLeft">
	    					<div class="text-box">
	    						<div class="copy1">featured program</div>
	    						<div class="copy2">Quiet Leadership Challenge</div>
	    						<div class="copy3">5-day | 20-24 March 2016</div>
	    						<div class="copy3">Margaret River</div>
	    						<div class="sep"></div>
	    						<a href="#"><div class="find-btn">find out more</div></a>
	    					</div>
	    				</div>
	    				<div class="testi-tile-sm tile-bg2 wow fadeInLeft">
	    					<div class="sm-copy1">FROM THE BLOG</div>
	    					<div class="sm-copy2">Outback on<br>Everest!</div>
	    					<div class="sm-copy3">Nam commodo scelerisque efficitur. Morbi viverra vehicula vulputate. Sed sagittis, enim eu luctus malesuada, nunc mauris congue ium, augue sed luctus pulvinar, libero loremvarius.</div>
	    					<div class="sm-sep"></div>
	    					<a href="#"><div class="sm-find">find out more</div></a>
	    				</div>

	    			</div>
	    			<div class="testimonail-tile right">
	    				<div class="testi-tile-sm tile-bg3 wow fadeInRight">
	    					<div class="sm-copy1">FROM THE BLOG</div>
	    					<div class="sm-copy2">Outback on<br>Everest!</div>
	    					<div class="sm-copy3">Nam commodo scelerisque efficitur. Morbi viverra vehicula vulputate. Sed sagittis, enim eu luctus malesuada, nunc mauris congue ium, augue sed luctus pulvinar, libero loremvarius.</div>
	    					<div class="sm-sep"></div>
	    					<a href="#"><div class="sm-find">find out more</div></a>
	    				</div>
	    				<div class="testi-tile-big tile-bg4 wow fadeInRight">
	    					<div class="text-box">
	    						<div class="copy1 bold">featured program</div>
	    						<div class="sep"></div>
	    						<a href="#"><div class="find-btn">find out more</div></a>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
    		</div>
    	</section>
    	<!-- events -->
    	<section class="events">
    		<div class="alyka-container clearfix">
    			<div class="col-sm-6">
    				<h2 class="head2-bold">upcoming events</h2>
    				<div class="event-row clearfix">
    					<div class="event-title">Ladies who Lunch</div>
    					<a href="#"><div class="alyka-btn book-btn">book now</div></a>
    				</div>
    				<div class="event-row clearfix">
    					<div class="event-title">Local Business Awards</div>
    					<a href="#"><div class="book-btn sold">sold out</div></a>
    				</div>
    				<div class="event-row clearfix">
    					<div class="event-title">Networking Lunch</div>
    					<a href="#"><div class="alyka-btn book-btn">book now</div></a>
    				</div>
    				<div class="event-row clearfix">
    					<div class="event-title">Women Only - Kokoda Trek</div>
    					<a href="#"><div class="alyka-btn book-btn">book now</div></a>
    				</div>
    				<div class="event-row nb clearfix">
    					<div class="event-title">Ladies who Lunch</div>
    					<a href="#"><div class="alyka-btn book-btn">book now</div></a>
    				</div>
    			</div>
    			<div class="col-sm-6">
    				<div class="social-tabs-con">
    					<div class="tabs fb active" data-tab="fb-content"></div>
    					<div class="tabs tw" data-tab="tw-content"></div>
    					<div class="tabs li" data-tab="li-content"></div>
    					<div class="tabs insta" data-tab="insta-content"></div>
    				</div>
    				<!-- facebook feeds -->
    				<div class="social-content fadeInUp animated fb-content active">
    					<div class="fb-page" data-href="https://www.facebook.com/outbackin.com.au/" data-tabs="timeline" data-width="500" data-height="320" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/outbackin.com.au/"><a href="https://www.facebook.com/outbackin.com.au/"></a></blockquote></div></div>
    				</div>

    				<!-- twitter feeds -->
    				<div class="social-content fadeInUp animated tw-content">
    					<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/OutbackIntWA" data-widget-id="739325271359135744">Tweets by @OutbackIntWA</a>
    				</div>

    				<!-- linkedIn company profile -->
    				<div class="social-content fadeInUp animated li-content">
						<script type="IN/CompanyProfile" data-id="829859" data-format="inline"></script>
    				</div>

    				<!-- instagram post -->
    				<div class="social-content fadeInUp animated insta-content">
    					<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:37.5% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BEV2XTcJbm_/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">A Day in the Life: Hiding clues for paramilitary training at Cape Leeuwin Waterwheel @margaretriver</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A photo posted by Outback Initiatives (@outbackinoz) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-04-18T12:20:55+00:00">Apr 18, 2016 at 5:20am PDT</time></p></div></blockquote>
    				</div>
    			</div>
    		</div>
    	</section>
    	<div class="awards-con">
	    	<div class="alyka-container">
				<div class="award"><img src="img/awards/award1.jpg"></div>
				<div class="award"><img src="img/awards/award2.jpg"></div>
				<div class="award"><img src="img/awards/award3.jpg"></div>
				<div class="award"><img src="img/awards/award1.jpg"></div>
				<div class="award"><img src="img/awards/award2.jpg"></div>
			</div>
		</div>
    </div>

<!--     Ladies who Lunch
Local Business Awards


Ladies who Lunch -->

<?php include 'inc/footer.php' ?>
