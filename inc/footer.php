		<footer>
			<div class="footer-content">
				<div class="alyka-container clearfix">
					<div class="col-md-6">
						<div class="links-col">
							<a href="#"><div class="link head">Home</div></a>
							<a href="#"><div class="link head">about</div></a>
							<a href="#"><div class="link">Team</div></a>
							<a href="#"><div class="link">case studies</div></a>
						</div>
						<div class="links-col">
							<a href="#"><div class="link head">service enterprise</div></a>
							<a href="#"><div class="link">Corporate</div></a>
							<a href="#"><div class="link">Agribusiness</div></a>
							<a href="#"><div class="link">Government</div></a>
							<a href="#"><div class="link">Solo Participants</div></a>
							<a href="#"><div class="link">Individuals</div></a>
							<a href="#"><div class="link">Woman & Youth</div></a>
						</div>
						<div class="links-col">
							<a href="#"><div class="link head">programs</div></a>
							<a href="#"><div class="link">Location</div></a>
							<a href="#"><div class="link">Upcoming</div></a>
							<a href="#"><div class="link">Registration</div></a>
							<a href="#"><div class="link">FAQ</div></a>
						</div>
						<div class="links-col">
							<a href="#"><div class="link head">Partners</div></a>
							<a href="#"><div class="link head">news</div></a>
							<a href="#"><div class="link">Upcoming</div></a>
							<a href="#"><div class="link">Blog</div></a>
							<a href="#"><div class="link">Photo Gallery</div></a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="subscribe">subscribe to our newsletter</div>
						<div>
							<input class="sub-name" type="text" name="name" placeholder="Name">
							<input class="sub-email" type="text" name="email" placeholder="Email">
						</div>
						<div class="alyka-btn-drk sub-btn">submit</div>
						<div class="error-alert"></div>
					</div>
					<div class="col-md-2 np">
						<a href="#" target="_blank"><div class="social-ft fb"></div></a>
						<a href="#" target="_blank"><div class="social-ft tw"></div></a>
						<a href="#" target="_blank"><div class="social-ft li"></div></a>
						<a href="#" target="_blank"><div class="social-ft insta"></div></a>
						<a href="#" target="_blank"><div class="social-ft yt"></div></a>
					</div>
				</div>
			</div>
			<div class="footer-copyright clearfix">
				<div class="alyka-container">
					<div class="copyright-con">
						<div class="copyright">Copyright 2016</div>
						<div class="designedby">website design alyka</div>
					</div>
				</div>
			</div>
		</footer>

        <script src="js/vendor/jquery-1.11.3.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/SmoothScroll.js"></script>
        <script src="js/swiper.jquery.js"></script>
        <script src="js/wow.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.mixitup.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/api.js"></script>
        <script>
          (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>

		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
		<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        
    </body>
</html>