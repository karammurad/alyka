<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/swiper.css">
        <link rel="stylesheet" href="css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>

    <header>
        <div class="alyka-container clearfix">
             <div class="top-bar clearfix">
                <div class="menu-nav-btn showMobile">
                    <div class="bars top"></div>
                    <div class="bars middle"></div>
                    <div class="bars bottom"></div>
                </div>
                <div class="logo"></div>
                <div class="search-con">
                    <div class="tel"></span> 1300 532 337</div>
                    <div class="search-box">
                        <input type="text" name="search" placeholder="Search">
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-container">
            <div class="alyka-container clearfix">
                <div class="nav">
                    <ul>
                        <a href=""><li><img src="img/icons/home.png"></li></a>
                        <a href=""><li>About Us</li></a>
                        <a href=""><li>Services</li></a>
                        <a href=""><li>Programs</li></a>
                        <a href=""><li>Partners</li></a>
                        <a href=""><li>News</li></a>
                        <a href=""><li>Contact Us</li></a>
                    </ul>
                </div>
            </div>    
        </div>
    </header>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
    <![endif]-->