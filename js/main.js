$(document).ready(function(){

	$('.menu-nav-btn').click(function(){
		$('.nav-container, .search-con').toggleClass('slide-left');
		$('.bars').toggleClass('active');
		$('body').toggleClass('body-overflow');
	})


	// social tabs
	$('.tabs').click(function(){
		$('.social-content, .tabs').removeClass('active');
		$('.'+$(this).data('tab')).addClass('active');
		$(this).addClass('active')
	})

	// subscribtion form
	$('.sub-btn').click(function(){
		var name = $('.sub-name').val();
		var email = $('.sub-email').val();
		var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;


		if(name == ''){
            $('.error-alert').html('Please type your name!')
            $('.sub-name').focus();
            return false;
        }
		if(email == ''){
            $('.error-alert').html('Please type your email!')
            $('.sub-email').focus();
            return false;
        }
        if ( !(filter.test(email)) ){
        	$('.error-alert').html('Invalid email!')
            $('.sub-email').focus();
        	return false;
        }
        else{
        	// console.log(form_val)
        	$.ajax({
        		type:"POST",
        		dataTyep:'json',
        		url:'index.php',
        		data:({name:name, email:email}),
        		success:function(res){
        			$('footer input').val('')
        			$('.error-alert').html('Thanks for your subscribtion!')
        			console.log(res)
        		},
        		error:function(e){
        			console.log(e)
        		}
        	})
        }

	})


	// Facebook initialization
    window.fbAsyncInit = function() {
      FB.init({	
        appId      : '283790401962532',
        xfbml      : true,
        version    : 'v2.5'
      });

      $('.share-icons.fb').click(function(){
        facebookShare();
      })

      $('.share-icons.tw').click(function(){
        twitterShare();
      })
      $('.share-icons.gplus').click(function(){
        googleShare();
      })


    }
})

function facebookShare(){
  var scroll = $(window).scrollTop();
  FB.ui({
      method: 'share',
      href: window.location.href,
      // description:$('.FBShare').attr('data-desc'), 
      // title:$('.FBShare').attr('data-title'),
      // caption:$('.FBShare').attr('data-caption'),
      // picture:window.location.href+'/static/images/img-home.jpg'
  })
}
function twitterShare(){
    loc = window.location.href;
    title = $('.offer-details .title').attr('data-tw');
    final_url = 'http://twitter.com/share?url=' + loc + '&text=' + title + '&';
    window.open(final_url, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
}

function googleShare(){
	loc = window.location.href;
  	window.open('https://plus.google.com/share?url='+loc,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600')

}