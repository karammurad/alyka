// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());






$(window).load(function(){
    // swiper initialization
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.assel.arrow-left',
        prevButton: '.assel.arrow-right',
        paginationClickable: true,
        autoplay:6000,
        speed:500,
        loop: true,
        grabCursor:true,
        // effect:'fade'
    });
    if ($('.has-slider').length > 0) {
        $('.swiper-slide.swiper-slide-active').find('.slider-head, .slider-btn').show()
        swiper.on('slideChangeStart', function () {
            // console.log('slide change start');
            $('.swiper-slide .slider-head').hide()
            $('.swiper-slide.swiper-slide-active').find('.slider-head').show()
            swiper.startAutoplay();
        });
    }

    // swiper initialization
    var swiper = new Swiper('.swiper-container-testimonials', {
        nextButton: '.assel.arrow-left.testi',
        prevButton: '.assel.arrow-right.testi',
        paginationClickable: true,
        // autoplay:6000,
        speed:500,
        loop: true,
        grabCursor:true,
        // effect:'fade'
    });

    // wow.js init
    var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       250,          // distance to the element when triggering the animation (default is 0)
        mobile:       false,       // trigger animations on mobile devices (default is true)
        live:         false,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        // scrollContainer: null // optional scroll container selector, otherwise use window
      }
    );
    wow.init();
})
